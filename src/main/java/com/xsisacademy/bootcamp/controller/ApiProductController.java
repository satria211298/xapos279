package com.xsisacademy.bootcamp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.bootcamp.model.Product;
import com.xsisacademy.bootcamp.repository.ProductRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/product")
public class ApiProductController {

	@Autowired
	private ProductRepository productRepository;

	@GetMapping
	public ResponseEntity<List<Product>> getAllVariant() {
		try {
			List<Product> product = this.productRepository.findByProduct();
			return new ResponseEntity<>(product, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("/add")
	public ResponseEntity<Object> saveProduct(@RequestBody Product product) {
		product.setIsActive(true);
		Product productData = this.productRepository.save(product);
		if (productData.equals(product)) {
			return new ResponseEntity<Object>("Save Data Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<List<Product>> getProductById(@PathVariable("id") Long id) {
		try {
			Optional<Product> productById = this.productRepository.findById(id);
			if (productById.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(productById, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Product>>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("/edit/{id}")
	public ResponseEntity<Object> editProductById(@RequestBody Product product, @PathVariable("id") Long id) {
		try {
			Optional<Product> getProductById = this.productRepository.findById(id);
			product.setIsActive(true);
			if (getProductById.isPresent()) {
				product.setId(id);
				this.productRepository.save(product);
				return new ResponseEntity<>("Update Data Success", HttpStatus.OK);
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("/delete/{id}")
	public ResponseEntity<Object> deleteProductById(@PathVariable("id") Long Id) {
		try {
			Optional<Product> getProductById = this.productRepository.findById(Id);
			if (getProductById.isPresent()) {
				Product product = new Product();
				product.setId(Id);
				this.productRepository.deleteProductById(Id);
				return new ResponseEntity<>("Delete Data Success", HttpStatus.OK);
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
		}
	}

}
