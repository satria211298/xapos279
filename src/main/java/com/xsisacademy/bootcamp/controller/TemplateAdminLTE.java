package com.xsisacademy.bootcamp.controller;

import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller //anotasi
@RequestMapping("/")
public class TemplateAdminLTE {
	
	@GetMapping("index2")
	public String index() {
		return "index2";
	}

}
