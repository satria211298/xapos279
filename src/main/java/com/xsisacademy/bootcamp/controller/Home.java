package com.xsisacademy.bootcamp.controller;

import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller //anotasi
@RequestMapping("/")
public class Home {
	
	@GetMapping("index")
	public String index() {
		return "index";
	}
	
	@GetMapping("form")
	public String form() {
		return "form";
	}
	
	@GetMapping("kalkulator1")
	public String kalkulator1() {
		return "kalkulator1";
	}
	
	@GetMapping("kalkulator2")
	public String kalkulator2() {
		return "kalkulator2";
	}

}
