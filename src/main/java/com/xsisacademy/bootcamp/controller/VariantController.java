package com.xsisacademy.bootcamp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xsisacademy.bootcamp.model.Category;
import com.xsisacademy.bootcamp.model.Variant;
import com.xsisacademy.bootcamp.repository.CategoryRepository;
import com.xsisacademy.bootcamp.repository.VariantRepository;

@Controller
@RequestMapping("/variant/")
public class VariantController {

	@Autowired
	private VariantRepository variantRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("variant/index");
		List<Variant> listvariant = this.variantRepository.findAll();
		view.addObject("listvariant", listvariant);
		return view;
	}
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("variant/indexapi");
		return view;
	}

	@GetMapping("addform")
	public ModelAndView addform() {
		ModelAndView view = new ModelAndView("variant/addform");
		Variant variant = new Variant();
		view.addObject("variant", variant);
		
		List<Category> listcategory = this.categoryRepository.findAll();
		view.addObject("listcategory", listcategory);
		return view;
	}

	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Variant variant, BindingResult result) {
		if (!result.hasErrors()) {
			this.variantRepository.save(variant);
		}
		return new ModelAndView("redirect:/variant/index");
	}

	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("variant/addform");
		Variant variant = this.variantRepository.findById(id).orElse(null);
		view.addObject("variant", variant);
		List<Category> listcategory = this.categoryRepository.findAll();
		view.addObject("listcategory", listcategory);
		return view;
	}

	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		if (id != null) {
			this.variantRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/variant/index");
	}

}
